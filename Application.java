public class Application
{
	public static void main(String[]args)
	{
		Student TonyDo = new Student();
		
		TonyDo.name = "Tony";
		TonyDo.age = 18;
		TonyDo.gender = "Male";
		
		System.out.println(TonyDo.name + ", " + TonyDo.age + ", " + TonyDo.gender);
		
		Student Nson = new Student();
		Nson.name = "Nson";
		Nson.age = 17;
		Nson.gender = "Male";
		
		System.out.println(Nson.name + ", " + Nson.age + ", " + Nson.gender);
		
		Nson.sayAge();
		TonyDo.sayAge();
		
		Student[] section3 = new Student[3];
		
		section3[0] = TonyDo;
		section3[1] = Nson;
		section3[2] = new Student();
		section3[2].age = 14;
		section3[2].gender = "Female";
		section3[2].name = "Emilia";
		System.out.println(section3[0].age);
		System.out.println(section3[2].age);
		System.out.println(section3[2].gender);
		
	}
	
}